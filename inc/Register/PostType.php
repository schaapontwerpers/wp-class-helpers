<?php

namespace WordPressClassHelpers\Register;

use WordPressPluginAPI\ActionHook;

abstract class PostType implements ActionHook
{
    /**
     * All arguments used for registering
     */
    protected $args;

    /**
     * All label information for the post type
     */
    protected $labels;

    /**
     * Name of the post type to register
     */
    protected $name;

    /**
     * Run functions to set labels and args
     */
    public function __construct()
    {
        $this->setName();
        $this->setLabels();
        $this->setArgs();
    }

    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return ['init' => ['register', 0, 10]];
    }

    /**
     * Register post type
     */
    public function register()
    {
        register_post_type($this->name, $this->args);
    }

    /**
     * Set arguments
     */
    abstract protected function setArgs();

    /**
     * Set labels
     */
    abstract protected function setLabels();

    /**
     * Set name of taxonomy
     */
    abstract protected function setName();
}
