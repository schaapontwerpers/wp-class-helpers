<?php

namespace WordPressClassHelpers\Register;

use WordPressPluginAPI\ActionHook;

abstract class Taxonomy implements ActionHook
{
    /**
     * Arguments belonging to the taxonomy
     */
    protected $args;

    /**
     * Labels belonging to the taxonomy
     */
    protected $labels;

    /**
     * Name of the taxonomy to register
     */
    protected $name;

    /**
     * Post Types to register the taxonomy to
     */
    protected $postTypes;

    /**
     * Run functions to set labels and args
     */
    public function __construct()
    {
        $this->setName();
        $this->setPostTypes();
        $this->setLabels();
        $this->setArgs();
    }

    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return ['init' => ['register', 0, 10]];
    }

    /**
     * Register taxonomy
     */
    public function register()
    {
        register_taxonomy($this->name, $this->postTypes, $this->args);
    }

    /**
     * Set arguments
     */
    abstract protected function setArgs();

    /**
     * Set labels
     */
    abstract protected function setLabels();

    /**
     * Set name of taxonomy
     */
    abstract protected function setName();

    /**
     * Determine post type to register to
     */
    abstract protected function setPostTypes();
}
