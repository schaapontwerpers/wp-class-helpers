<?php

namespace WordPressClassHelpers\Register;

use WordPressPluginAPI\ActionHook;

abstract class RestRoute implements ActionHook
{
    protected $args = [];

    protected $methods = \WP_REST_SERVER::READABLE;

    protected $route;

    /**
     * Run functions to set labels and args
     */
    public function __construct()
    {
        $this->setRoute();
    }

    /**
     * Add filters
     */
    public static function getActions(): array
    {
        return array('rest_api_init' => 'register');
    }

    /**
     * Add filters
     */
    public function register()
    {
        register_rest_route(
            defined('REST_NAMESPACE') ? REST_NAMESPACE : 'sdc/v1',
            $this->route,
            array(
                'methods' => $this->methods,
                'callback' => array($this, 'getCallback'),
                'args' => $this->args,
                'permission_callback' => array($this, 'getPermissionCallback'),
            ),
            true
        );
    }

    /**
     * Get callback
     */
    abstract public function getCallback(\WP_REST_Request $request);

    /**
     * Get permission callback
     */
    abstract public function getPermissionCallback(): bool;

    /**
     * Set name of route
     */
    abstract protected function setRoute();
}
