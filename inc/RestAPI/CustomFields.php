<?php

namespace WordPressClassHelpers\RestAPI;

use WordPressPluginAPI\ActionHook;
use WordPressPluginAPI\FilterHook;

abstract class CustomFields implements ActionHook, FilterHook
{
    /**
     * Actions to run through the Plugin API
     */
    protected static $actions = [
        'rest_api_init' => 'registerFields',
    ];

    /**
     * Filters to run through the Plugin API
     */
    protected static $filters = [
        'rest_prepare_revision' => ['registerPreviewFields', 10, 3],
    ];

    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return static::$actions;
    }

    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        return static::$filters;
    }

    /**
     * Register all fields
     */
    abstract public function registerFields();

    /**
     * Register fields for preview
     */
    abstract public function registerPreviewFields(
        \WP_REST_Response $response,
        \WP_Post $post,
        \WP_REST_Request $request,
    ): \WP_REST_Response;
}
